import locale
import requests
import discord
import json
import flag
import re
import os

locale.setlocale(locale.LC_ALL, "")
TOKEN = os.getenv('DISCORD_TOKEN')

#bingCovid = requests.get("https://bing.com/covid/data")

with open("states.json",'r') as statefile:
	statejson = json.load(statefile)

def bingOutput(region):
	outFormat = ""
	addFlag = ""
	#separate all values and change any none/null values to 0
	outList = []
	outList.append(region['displayName'])
	outList.append(region['totalConfirmed'])
	outList.append(region['totalConfirmedDelta'])
	outList.append(region['totalDeaths'])
	outList.append(region['totalDeathsDelta'])
	outList.append(region['totalRecovered'])
	outList = [0 if x == None else x for x in outList]
	
	#get flag emoji if global country
	if region['parentId'] == "world":
		uniFlags = requests.get("https://flagpedia.net/download/country-codes-case-upper.json")
		for key, value in uniFlags.json().items():
			if value == outList[0]:
				countryCode = key
		addFlag = flag.flag(countryCode)

	#return all values and add commas to number values
	outFormat = f"{addFlag} {outList[0]} Covid-19 Statistics\n"
	outFormat += "🤢 Total Cases: " + str(f"{outList[1]:n}")
	outFormat += " -- New Cases: " + str(f"{outList[2]:n}")
	outFormat += "  💀 Total Deaths: " + str(f"{outList[3]:n}")
	outFormat += " -- New Deaths: " + str(f"{outList[4]:n}")
	outFormat += "  😅 Total Recovered: " + str(f"{outList[5]:n}")
	return outFormat

def usStateStats(state):
	bingCovid = requests.get("https://bing.com/covid/data")
	stateOut = ""
	getState = state.upper()
	try:
		getState = statejson['states'][(getState[:2])]
		for i in bingCovid.json()['areas'][0]['areas']:
			if i['id'] == getState:
				stateOut = bingOutput(i)
	except:
		stateOut = getState + " is not in the state list.  Try again."
	return stateOut

def usCountyStats(state, county):
	bingCovid = requests.get("https://bing.com/covid/data")
	countyOut = ""
	getState = state.upper()
	try:
		getState = statejson['states'][(getState[:2])]
		getCounty = county.lower() + "_" + getState
		for i in bingCovid.json()['areas'][0]['areas']:
			if i['id'] == getState:
				for sc in i['areas']:
					if sc['id'] == getCounty:
						countyOut = bingOutput(sc)
	except:
		countyOut = getCounty + " is not in the county list.  Try again."
	return countyOut

def countryStats(country):
	bingCovid = requests.get("https://bing.com/covid/data")
	countryOut = ""
	if country == "":
		i = bingCovid.json()
		countryOut = bingOutput(i)
	else:
		try:
			for i in bingCovid.json()['areas']:
				if i['id'] == country:
					countryOut = bingOutput(i)
		except:
			countryOut = country + " is not in the country list. Try again."
	return countryOut

def main():
	bingCommands = "Bing Covid-19 Lookup Commands:\n"
	bingCommands += "bingus  --Returns USA statistics updated from Bing tracker\n"
	bingCommands += "bingus <XX>  --Returns US State statistics using 2-letter abbreviation\n"
	bingCommands += "bingint  --Returns Global total statistics updated from Bing tracker\n"
	bingCommands += "bingint <countryname>  --Returns specific Country statistics using full country name"

	client = discord.Client()
	bingUSPattern = re.compile("bingus")
	bingIntPattern = re.compile("bingint")

	#bingLookup = input("Bing Covid: ").lower()
	
	@client.event
	async def on_ready():
		print(f"{client.user} has connected to Discord!")
	
	@client.event
	async def on_message(bingLookup):
		if bingLookup.content.lower() == "binghelp":
			await bingLookup.channel.send(bingCommands)
		
		elif bingUSPattern.match(bingLookup.content.lower()):
			getState = bingLookup.content[7:]
			if getState == '':
				print("Country: unitedstates")
				await bingLookup.channel.send(countryStats("unitedstates"))
			elif " ".join(getState.split()).count(" ") > 0:
				getCounty = " ".join(getState.split()).split()[1]
				print(f"State: {getState} - County: {getCounty}")
				await bingLookup.channel.send(usCountyStats(getState, getCounty))
			else:
				print(" ".join(getState.split()).count(" "))
				print(f"State: {getState}")
				await bingLookup.channel.send(usStateStats(getState))
		
		elif bingIntPattern.match(bingLookup.content.lower()):
			getCountry = bingLookup.content[8:].lower().replace(" ", "")
			print(f"Country: {getCountry}")
			await bingLookup.channel.send(countryStats(getCountry))
	
	client.run(TOKEN)


if __name__ == '__main__':
	main()
