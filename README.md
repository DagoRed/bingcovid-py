# BingCovid

A python Discord bot to report some findings of the COVID-19 virus, using the BING API.

## Pre-reqs

It is best to use virtual env. Once started use `pip install -r requirements.txt`. 

## Use

1. Create a Discord bot through the [Discord Developer portal](https://discordapp.com/developers/applications/).
1. Copy unique Discord key to a file named `.env` with `export DISCORD_TOKEN=<key>` as it's contents. 
1. `source .env`
1. Start bot

 
 bingbot accepts 4 arguments:

`bingint` -Returns "Total Global" statistics

`bingint countryname` -Returns "Total Country" statistics

`bingus` -Returns "Total United States" statistics

`bingus statename` -Returns "Total US State" statistics

`bingus statename countyname` -Returns "Total County stats within given state **(NEW)**

### Notes

Possible reportable statistics for each query, need to modify output function to include more.

    id
    displayName
    areas
    totalConfirmed
    totalDeaths
    totalRecovered
    totalRecoveredDelta
    totalDeathsDelta
    totalConfirmedDelta

## New

Converted into Discord bot.  
Added US counties within a state.  
Added numerical commas.  
Added iconography.  
Added country flags.  